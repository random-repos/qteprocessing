//--------------------
//QTE 
//if you downloaded this directly from our repository, this project should contain a 'tutorial project'
//run the project to see the tutorial explaining how to use QTE
//please look at data/video.txt for specific comments on how to make your own FMV
//
//QTE is a FMV game engine developed by Peter Lu for the QTE Game Jam at UCLA Game Lab
//http://games.ucla.edu/resource/quick-time-event/
//--------------------

import processing.video.*;
import java.util.*;

//change these variables to modify the colors during the choosing phase
color mOnHoverColor = color(255, 255, 255, 255);
color mNormalColor = color(200, 200, 200, 255);
color mBackgroundBoxColor = color(0, 0, 0, 100);
int mTextSize = 32; //this one can  be changed in the video.txt file
int mVertPadding = 5;
int mHorizPadding = 10;

//important stuff needed for the program to compile and work properly ;)
Movie mov;
PVector mScreenSize = new PVector(640,480);
Map<String, ClipGroup> mClips;
ClipGroup mActiveClip = null;

//variable needed for mouse released behaviour
boolean mWasMouseDown = false;

//this function determines behaviour at the scene transitions
//it's called during every frame we are in a transition phase
void sceneTransition()
{
    //draw a background box so the text is more visible
    //we draw individual boxes now
    //fill(mBackgroundBoxColor);
    //rect(10, 10, width-20, height-20);
    
    //if there are transitions
    int numClips = mActiveClip.transitions.size();
    if (numClips > 0)
    {
      int space = (int)(mScreenSize.y/numClips);
      for (int i = 0; i < numClips; i++) //for each of our transition choices
      {
        
        //draw individual box behind the text
        fill(mBackgroundBoxColor);
        rect(mHorizPadding, (i)*space+mVertPadding,width-mHorizPadding*2,space-mVertPadding*2);
    
        if (mouseY > i*space && mouseY <= (i+1)*space) //if we are hovering over this choice
        {
          if (!mousePressed && mWasMouseDown) //if we release click, then load the clip and transition to it
          {
            mActiveClip = mClips.get(mActiveClip.transitions.get(i).transition);
            if (mActiveClip == null) //if the transition clip does not actually exist
              logError("transition " + mActiveClip.transitions.get(i).transition + " does not exist.");
            playClip(mActiveClip.fileName);
            break;
          } 
          fill(mOnHoverColor); //color it special
        } 
        else 
          fill(mNormalColor); //color it normal
        text(mActiveClip.transitions.get(i).prompt, mHorizPadding*2, (i)*space+mVertPadding*2,width-mHorizPadding*4,space-mVertPadding*4);
      }
    }
    else { //if there are no transitions
      exit(); //quit the game with no warning!
    }
}

//you probably don't want to mess with anything below here
void setup() {
  
  //setup variables
  mClips = new HashMap<String, ClipGroup>();

  //parse the info file
  BufferedReader reader;
  reader = createReader("video.txt");
  try {
    String line = reader.readLine();
    String[] words = null;
    ClipGroup operating = null;
    while (line != null)
    {
      words = line.split(" ");
      if (words.length == 0 || words[0].startsWith("#")) {
      } //don't do anything
      else if (words[0].equals("SIZE")){
        if(!isInteger(words[1]))
          mScreenSize = new PVector(displayWidth,displayHeight);
        else
          mScreenSize = new PVector( Integer.parseInt(words[1]), Integer.parseInt(words[2]));
      }
      else if (words[0].equals("FONTSIZE")) {
        mTextSize = Integer.parseInt(words[1]);
      }
      else if (words[0].equals("SCENE")) {
        operating = new ClipGroup();
        operating.name = words[1];
        if(words.length > 2)
          operating.fileName = words[2];
        else
          operating.fileName = "";
        if (mClips.size() == 0) 
          mActiveClip = operating;
        mClips.put(operating.name, operating);
      }
      else if(words[0].equals("AUTO")){
        Trans tr = new Trans();
        tr.transition = words[1];
        operating.auto = tr;
      }
      else if (words[0].equals("PROMPT")) {
        if (operating != null) {
          Trans tr = new Trans();
          tr.transition = words[1];
          tr.prompt = line.substring(8 + words[1].length());
          operating.transitions.add(tr);
        }
        else {
          logError("parse error on line <" + line + ">, I see transitions but no assosciated clip");
        }
      }
      else if(!line.trim().isEmpty()){
        operating.transitions.get(operating.transitions.size()-1).prompt += "\n" + line;
      }

      line = reader.readLine();
    }
  } 
  catch (IOException e) {
    e.printStackTrace();
  }

  //log an error if there are no clips in the file
  if (mActiveClip == null)
    logError("file contains no clips");

  //setup the screen
  size((int)mScreenSize.x, (int)mScreenSize.y);

  //setup processing default state
  noStroke();
  background(0);
  textSize(mTextSize);

  //start the clip
  playClip(mActiveClip.fileName);
}

void draw() {
  if(mov != null)
    image(mov, 0, 0, width, height);
  //println(mov.time() + " " + mov.duration());
  if (isInTransition()){
    if(mActiveClip.auto != null)
    {
      mActiveClip = mClips.get(mActiveClip.auto.transition);
      playClip(mActiveClip.fileName);
    }
    else 
      sceneTransition();
  }
  mWasMouseDown = mousePressed;
}

void keyReleased()
{
  if(keyCode == ENTER)
    if(mov != null)
    {
      mov.jump(mov.duration()-0.02f);
      mov.play();
    }
}

Boolean isInTransition()  
{
  if(mov == null) return true;
  return mov.time() >= mov.duration() - 0.05f;
  //return mov.isPlaying(); this isn't available for whatever reason
}

//this is all we need to do to transition to a new clip  
void playClip(String clipName)
{
  if(clipName != "")
  {
    mov = new Movie(this, clipName);
    mov.play();
  }
}


//function for logging errors :)
void logError(String msg)
{
  //TODO log file maybe??
  print(msg);
}

boolean isInteger(String str) {
    return str.matches("^-?[0-9]+(\\.[0-9]+)?$");
}

//callback for movies, required for movies to play properly
void movieEvent(Movie m) {
  m.read();
}

//this class represents a transition
class Trans {
  public String transition;
  public String prompt;
}

//this class represents a scene
class ClipGroup {
  public String name;
  public String fileName;
  public Movie clip;
  public List<Trans> transitions;
  public Trans auto;
  public ClipGroup() {
    clip = null;
    transitions = new ArrayList<Trans>();
    auto = null;
  }
}

